FROM node:22.2.0-alpine

WORKDIR /app

COPY package.json yarn.lock ./

COPY dist ./dist

COPY .env ./

RUN echo "Asia/Krasnoyarsk" > /etc/timezone && \
        yarn install --production

CMD ["node", "dist/main"]