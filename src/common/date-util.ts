import { format } from 'date-fns';
import { ru } from 'date-fns/locale';

export function formatRussianDate(date) {
  return format(date, "d MMMM yyyy'г'", { locale: ru });
}
