export function getPage(page: number, content: any[]) {
  const startIndex = page * 10;
  const endIndex = startIndex + 10;
  return content.slice(startIndex, endIndex);
}

export function getTotalPages(content: any[]): number {
  return Math.ceil(content.length / 10);
}
