import { Injectable, Logger } from '@nestjs/common';
import * as fs from 'fs/promises';
import * as fsSync from 'fs';
import * as path from 'path';

@Injectable()
export class FileService {
  public async readFile<Type>(filePath: string): Promise<Type> {
    try {
      if (this.fileExists(filePath)) {
        const data = await fs.readFile(filePath, 'utf8');
        return JSON.parse(data);
      }
    } catch (error) {
      Logger.error('Ошибка при чтении файла');
    }
  }

  public async saveFile(
    data: any,
    folderPath: string,
    fileName: string,
  ): Promise<void> {
    try {
      const isExist = this.fileExists(folderPath);
      if (!isExist) {
        Logger.log(`Создаем директорию ${folderPath}`);
        fsSync.mkdirSync(folderPath, { recursive: true });
      }
      const json = JSON.stringify(data, null, 2);
      fsSync.writeFileSync(path.join(folderPath, fileName), json);
    } catch (error) {
      console.error('Ошибка при сохранении файла');
    }
  }

  public async getFiles(folderPath: string): Promise<string[]> {
    return await fs.readdir(folderPath, { encoding: 'utf-8' });
  }

  private fileExists(path: string): boolean {
    try {
      fsSync.accessSync(path);
      return true;
    } catch {
      return false;
    }
  }

  public async dateOfCreate(filePath: string): Promise<Date> {
    try {
      const stats = await fs.stat(filePath);
      return stats.mtime; // Время последнего изменения
    } catch (error) {
      Logger.error(`Ошибка при получении информации о файле: ${error.message}`);
      return null;
    }
  }
}
