import { Module } from '@nestjs/common';
import { BotService } from './bot.service';
import { BotProvider } from './bot.provider';
import { TelegrafModule } from 'nestjs-telegraf';
import { ClientModule } from 'src/client/client.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { session } from 'telegraf';
import { Redis } from '@telegraf/session/redis';
import { PortfolioScene } from './scene/portfolio.scene';
import { SketchScene } from './scene/sketches.scene';

@Module({
  imports: [
    TelegrafModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        token: configService.get<string>('TOKEN'),
        middlewares: [
          session({
            store: Redis({
              url: configService.get<string>('REDIS_URL'),
              config: { database: 0 },
            }),
          }),
        ],
      }),
    }),
    ClientModule,
  ],
  controllers: [],
  providers: [BotProvider, BotService, PortfolioScene, SketchScene],
  exports: [BotProvider, BotService],
})
export class BotModule {}
