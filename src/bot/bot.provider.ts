import { Context } from 'telegraf';
import { BotService } from './bot.service';
import { Action, Ctx, Start, Update } from 'nestjs-telegraf';
import { CtxSession } from './type/session-ctx.type';
import { SceneType } from './type/scene.type';
import { portfolioKeyboard } from './keyboard/portfolio.keyboard';
import { Logger } from '@nestjs/common';

@Update()
export class BotProvider {
  constructor(private readonly botService: BotService) {}

  @Start()
  public greetings(@Ctx() ctx: Context & CtxSession): void {
    this.botService.greetings(ctx);
  }

  @Action('sketch')
  public async getSketch(@Ctx() ctx: Context & SceneType & CtxSession) {
    Logger.log(`${ctx.from.username} зашел в эскизы`);
    ctx.scene.enter('sketches');
  }

  @Action('schedule')
  public async getSchedule(@Ctx() ctx: Context) {
    Logger.log(`${ctx.from.username} зашел в расписание`);
    this.botService.getSchedule(ctx);
    ctx.editMessageText('Актуальное расписание');
  }

  @Action('portfolio')
  public async getPortfolio(@Ctx() ctx: Context & SceneType & CtxSession) {
    Logger.log(`${ctx.from.username} зашел в портфолио`);
    ctx.scene.enter('portfolio');
    ctx.session.page = 0;
    ctx.session.lastMessage = await ctx
      .editMessageText('Мои работы', {
        reply_markup: { inline_keyboard: portfolioKeyboard },
      })
      .then((msg: any) => msg.message_id);
  }
}
