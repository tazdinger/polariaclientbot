import { Action, Ctx, Scene } from 'nestjs-telegraf';
import { Context } from 'telegraf';
import { CtxSession } from '../type/session-ctx.type';
import { portfolioKeyboardWithoutView } from '../keyboard/portfolio.keyboard';
import { mainKeyboard } from '../keyboard/main.keyboard';
import { SceneType } from '../type/scene.type';
import { onePagesKeyboard, pagesKeyboard } from '../keyboard/pages.keyboard';
import { folder } from 'src/common/folder-path';
import { BotService } from '../bot.service';

@Scene('portfolio')
export class PortfolioScene {
  constructor(private readonly botService: BotService) {}

  private ON_PAGE = 6;

  @Action('view')
  public async getPortfolio(@Ctx() ctx: Context & CtxSession): Promise<void> {
    ctx.session.page = ctx.session?.page || 0;
    await ctx.deleteMessages(ctx.session.mediaMessageId).catch(() => null);
    await ctx.deleteMessage(ctx.session.lastMessage);
    ctx.session.mediaMessageId = [];

    const images = await this.botService.getMediaFiles(ctx, folder.portfolio);
    if (images.length === 0) {
      ctx.session.lastMessage = await ctx
        .reply('Загрузи сюда свои работы', {
          reply_markup: { inline_keyboard: portfolioKeyboardWithoutView },
        })
        .then((messId) => messId.message_id);
      return;
    }
    const totalPages = this.getTotalPages(images);
    if (totalPages === 1) {
      const arr = await ctx
        .replyWithMediaGroup(images)
        .then((messId: any) => messId.message_id);
      ctx.session.mediaMessageId = arr;
      ctx.session.lastMessage = await ctx
        .reply(`Страница 1`, {
          reply_markup: { inline_keyboard: onePagesKeyboard },
        })
        .then((messId) => messId.message_id);
      return;
    }
    const currentPage =
      ctx.session.page >= totalPages ? totalPages - 1 : ctx.session.page || 0;
    ctx.session.page = currentPage;
    const response = this.getPage(currentPage, images);
    const mediaMessageIds = await ctx.replyWithMediaGroup(response);
    const arr = [];
    mediaMessageIds.forEach((m) => arr.push(m.message_id));

    ctx.session.mediaMessageId = arr;
    ctx.session.lastMessage = await ctx
      .reply(`Страница ${currentPage + 1} (${totalPages})`, {
        reply_markup: { inline_keyboard: pagesKeyboard },
      })
      .then((messId) => messId.message_id);
  }

  public getPage(page: number, content: any[]) {
    const startIndex = page * this.ON_PAGE;
    const endIndex = startIndex + this.ON_PAGE;
    return content.slice(startIndex, endIndex);
  }

  public getTotalPages(content: any[]): number {
    return Math.ceil(content.length / this.ON_PAGE);
  }

  @Action('next')
  public async nextPage(@Ctx() ctx: Context & CtxSession) {
    ctx.session.page = ctx.session.page + 1;
    await this.getPortfolio(ctx);
  }

  @Action('previous')
  public async previousPage(@Ctx() ctx: Context & CtxSession) {
    ctx.session.page = ctx.session.page <= 0 ? 0 : ctx.session.page - 1;
    await this.getPortfolio(ctx);
  }

  @Action('backToScene')
  public async backToPortfolio(@Ctx() ctx: Context & CtxSession) {
    ctx.session.lastMessage = await ctx
      .editMessageText('Выберите пункт', {
        reply_markup: {
          inline_keyboard: mainKeyboard,
        },
      })
      .then((messId: any) => messId.message_id);
  }

  @Action('back')
  public async onCancel(@Ctx() ctx: Context & SceneType) {
    await ctx.editMessageText('Выберите пункт', {
      reply_markup: {
        inline_keyboard: mainKeyboard,
      },
    });
    await ctx.scene.leave();
  }

  delay(ms: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
}
