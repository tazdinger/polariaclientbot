import {
  sketchKeyboard,
  sketchKeyboardWithoutView,
} from '../keyboard/sketch.keyboard';
import { Action, Ctx, Scene, SceneEnter, SceneLeave } from 'nestjs-telegraf';
import { Context } from 'telegraf';
import { BotService } from '../bot.service';
import { Logger } from '@nestjs/common';
import { mainKeyboard } from '../keyboard/main.keyboard';
import { SceneType } from '../type/scene.type';
import { folder } from 'src/common/folder-path';
import { CtxSession } from '../type/session-ctx.type';
import { getPage, getTotalPages } from 'src/common/page-util';
import { onePagesKeyboard, pagesKeyboard } from '../keyboard/pages.keyboard';

@Scene('sketches')
export class SketchScene {
  constructor(private readonly botService: BotService) {}

  @SceneEnter()
  async onSceneEnter(@Ctx() ctx: Context & CtxSession) {
    ctx.session.sketchPage = 0;
    ctx.session.lastMessage = await ctx
      .editMessageText('Свободные эскизы', {
        reply_markup: {
          inline_keyboard: sketchKeyboard,
        },
      })
      .then((messId: any) => messId.message_id);
  }

  @Action('back')
  async onCancel(@Ctx() ctx: Context & SceneType) {
    await ctx.editMessageText('Выбери пункт', {
      reply_markup: {
        inline_keyboard: mainKeyboard,
      },
    });
    await ctx.scene.leave();
  }

  @SceneLeave()
  async onSceneLeave(@Ctx() ctx: Context) {
    Logger.log(`${ctx.from.username} вышел из эскизов`);
  }

  @Action('view')
  public async getSketch(ctx: Context & CtxSession): Promise<void> {
    ctx.session.sketchPage = ctx.session?.sketchPage || 0;
    await ctx.deleteMessages(ctx.session.sketchMessageId).catch(() => null);
    await ctx.deleteMessage(ctx.session.lastMessage);
    ctx.session.mediaMessageId = [];

    const images = await this.botService.getMediaFiles(ctx, folder.sketches);
    if (images.length === 0) {
      await ctx.editMessageText('Эскизов на данный момент нет :(', {
        reply_markup: { inline_keyboard: sketchKeyboardWithoutView },
      });
      return;
    }
    const totalPages = getTotalPages(images);
    if (totalPages === 1) {
      const arr = await ctx
        .replyWithMediaGroup(images)
        .then((messId: any) => messId.message_id);
      ctx.session.sketchMessageId = arr;
      ctx.session.lastMessage = await ctx
        .reply(`Страница 1`, {
          reply_markup: { inline_keyboard: onePagesKeyboard },
        })
        .then((messId) => messId.message_id);
      return;
    }
    const currentPage =
      ctx.session.sketchPage >= totalPages
        ? totalPages - 1
        : ctx.session.sketchPage || 0;
    ctx.session.sketchPage = currentPage;
    const response = getPage(currentPage, images);
    const mediaMessageIds = await ctx.replyWithMediaGroup(response);
    const arr = [];
    mediaMessageIds.forEach((m) => arr.push(m.message_id));

    ctx.session.sketchMessageId = arr;
    ctx.session.lastMessage = await ctx
      .reply(`Страница ${currentPage + 1} (${totalPages})`, {
        reply_markup: { inline_keyboard: pagesKeyboard },
      })
      .then((messId) => messId.message_id);
  }

  @Action('next')
  public async nextPage(@Ctx() ctx: Context & CtxSession) {
    ctx.session.sketchPage = ctx.session.sketchPage + 1;
    await this.getSketch(ctx);
  }

  @Action('previous')
  public async previousPage(@Ctx() ctx: Context & CtxSession) {
    ctx.session.sketchPage =
      ctx.session.sketchPage <= 0 ? 0 : ctx.session.sketchPage - 1;
    await this.getSketch(ctx);
  }

  @Action('backToScene')
  public async backToPortfolio(@Ctx() ctx: Context & CtxSession) {
    ctx.session.lastMessage = await ctx
      .editMessageText('Выберите пункт', {
        reply_markup: {
          inline_keyboard: mainKeyboard,
        },
      })
      .then((messId: any) => messId.message_id);
  }
}
