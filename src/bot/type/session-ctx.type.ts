export type CtxSession = {
  session: {
    mediaMessageId: number[];
    sketchMessageId: number[];
    page: number;
    sketchPage: number;
    lastMessage: number;
  };
};
