export type SceneType = {
  scene: {
    enter: (string) => void;
    leave: () => void;
  };
};
