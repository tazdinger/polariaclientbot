import { Injectable, Logger } from '@nestjs/common';
import { Context } from 'telegraf';
import { MediaGroup } from 'telegraf/typings/telegram-types';
import * as path from 'path';
import { ClientService } from 'src/client/client.service';
import { FileService } from 'src/file/file.service';
import { mainKeyboard } from './keyboard/main.keyboard';
import { formatRussianDate } from 'src/common/date-util';
import { folder } from 'src/common/folder-path';
import { CtxSession } from './type/session-ctx.type';
import { InputMediaPhoto } from 'telegraf/typings/core/types/typegram';

@Injectable()
export class BotService {
  constructor(
    private readonly clientService: ClientService,
    private readonly fileService: FileService,
  ) {}

  public async greetings(ctx: Context & CtxSession): Promise<void> {
    const id = ctx.chat.id.toString();
    const username = ctx.from.username;
    const currentClients = this.clientService.getClients();
    if (!currentClients[id]) {
      await this.clientService.addClient(id, username);
      Logger.log(`Пользователь ${username} с id ${id} присоединился!`);
    }
    await ctx.reply('Приветик!', {
      reply_markup: {
        inline_keyboard: mainKeyboard,
        resize_keyboard: true,
        one_time_keyboard: false,
      },
    });
  }

  public async getSketch(ctx: Context): Promise<void> {
    const images = await this.getImagesName(folder.sketches);
    if (images.length === 0) {
      ctx.reply('На данный момент эскизов нет :(', {
        reply_markup: {
          inline_keyboard: mainKeyboard,
        },
      });
      return;
    }
    const response = images.map((filePath, index) => {
      return {
        type: 'photo',
        media: { source: path.join(folder.sketches, filePath) },
        caption: index === 0 ? 'Свободные эскизы' : undefined,
      };
    });
    await ctx.replyWithMediaGroup(response as MediaGroup);
    await ctx.reply('Выберите пункт', {
      reply_markup: {
        inline_keyboard: mainKeyboard,
      },
    });
  }

  public async getSchedule(ctx: Context): Promise<void> {
    const images = await this.getImagesName(folder.schedule);
    if (images.length === 0) {
      ctx.reply('Нет свободных окошечек :(', {
        reply_markup: {
          inline_keyboard: mainKeyboard,
        },
      });
      return;
    }
    const createdAt = await this.fileService
      .dateOfCreate(path.join(folder.schedule, images[0]))
      .then((date) => formatRussianDate(date));
    const response = images.map((filePath, index) => {
      return {
        type: 'photo',
        media: { source: path.join(folder.schedule, filePath) },
        parse_mode: 'Markdown',
        caption:
          index === 0
            ? `Ближайшие окошечки\nАктуально: ${createdAt}\n_@polariatattoo_`
            : undefined,
      };
    });
    await ctx.replyWithMediaGroup(response as MediaGroup);
    await ctx.reply('Выберите пункт', {
      reply_markup: {
        inline_keyboard: mainKeyboard,
      },
    });
  }

  public async getMediaFiles(
    ctx: Context,
    folderPath: string,
  ): Promise<InputMediaPhoto[]> {
    const files = await this.fileService.getFiles(folderPath).catch(() => {
      return [] as string[];
    });
    if (files.length === 0) {
      return [];
    }
    const media: InputMediaPhoto[] = files.map((filePath) => {
      return {
        type: 'photo',
        media: { source: path.join(folderPath, filePath) },
      };
    });
    return media;
  }

  private async getImagesName(folderPath: string): Promise<string[]> {
    return await this.fileService.getFiles(folderPath).catch(() => {
      return [];
    });
  }
}
