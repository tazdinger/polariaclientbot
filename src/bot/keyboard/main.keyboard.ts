export const mainKeyboard = [
  [{ text: 'Мои работы', callback_data: 'portfolio' }],
  [
    { text: 'Эскизы', callback_data: 'sketch' },
    { text: 'Окошечки', callback_data: 'schedule' },
  ],
];
