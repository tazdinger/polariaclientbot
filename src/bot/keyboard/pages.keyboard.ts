export const pagesKeyboard = [
  [
    { text: 'Предыдущая', callback_data: 'previous' },
    { text: 'Следующая', callback_data: 'next' },
  ],
  [{ text: 'Назад', callback_data: 'backToScene' }],
];

export const onePagesKeyboard = [
  [{ text: 'Назад', callback_data: 'backToScene' }],
];
