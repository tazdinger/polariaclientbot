export const portfolioKeyboard = [
  [{ text: 'Посмотреть', callback_data: 'view' }],
  [{ text: 'Назад', callback_data: 'back' }],
];

export const portfolioKeyboardWithoutView = [
  [{ text: 'Назад', callback_data: 'back' }],
];
