import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import * as dotenv from 'dotenv';
import { ExtendedLogger } from './logger/extended-logger';

async function bootstrap() {
  dotenv.config();

  const PORT = process.env.PORT || 3000;
  const app = await NestFactory.create(AppModule, {
    logger: new ExtendedLogger(),
  });

  app.enableShutdownHooks();

  await app.listen(PORT, () =>
    Logger.log(`Server started on ${PORT} port`, 'MAIN'),
  );
}
bootstrap();
