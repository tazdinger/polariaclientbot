import { Module } from '@nestjs/common';
import { ClientService } from './client.service';
import { FileService } from 'src/file/file.service';

@Module({
  controllers: [],
  providers: [ClientService, FileService],
  exports: [ClientService, FileService],
})
export class ClientModule {}
