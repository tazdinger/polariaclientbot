import { FileService } from './../file/file.service';
import { Injectable, Logger } from '@nestjs/common';
import { ClientType } from './type/client.type';
import * as path from 'path';

@Injectable()
export class ClientService {
  constructor(private readonly fileService: FileService) {}

  private CLIENT_PATH = path.join(__dirname, '..', '..', 'static', 'clients');

  private async onModuleInit() {
    this.clients = await this.fileService.readFile<ClientType>(
      path.join(this.CLIENT_PATH, 'clients.json'),
    );
    if (!this.clients) {
      this.clients = {};
    }
    Logger.warn(`Клиентов: ${Object.keys(this.clients).length}`);
  }

  private clients: ClientType;

  public getClients(): ClientType {
    return this.clients;
  }

  public addClient(id: string, username: string) {
    this.clients[id] = { username, date: new Date() };
  }

  private async saveClientsToFile(): Promise<void> {
    Logger.log('Сохраняем...');
    await this.fileService.saveFile(
      this.clients,
      this.CLIENT_PATH,
      'clients.json',
    );
    Logger.warn(`Сохранено: ${Object.keys(this.clients).length}`);
  }

  private onApplicationShutdown(): void {
    Logger.warn('App shutdown...');
    this.saveClientsToFile();
  }
}
