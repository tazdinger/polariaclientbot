export type ClientType = {
  [key: string]: {
    username: string;
    subscriber?: boolean;
    date?: Date;
  };
};
