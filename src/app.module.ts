import { Module } from '@nestjs/common';
import { BotModule } from './bot/bot.module';
import { FileModule } from './file/file.module';
import { ClientModule } from './client/client.module';

@Module({
  imports: [BotModule, FileModule, ClientModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
