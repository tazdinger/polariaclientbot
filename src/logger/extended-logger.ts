import { ConsoleLogger } from '@nestjs/common';
import logger from './winston-logger';

export class ExtendedLogger extends ConsoleLogger {
  error(message: any, ...optionalParams: any[]) {
    logger.error(`ERROR [${optionalParams['stack']}]: ${message}`);
    super.error(message, ...optionalParams);
  }

  warn(message: any, ...optionalParams: any[]) {
    logger.warn(`WARN [${optionalParams['stack']}]: ${message}`);
    super.warn(message, ...optionalParams);
  }

  log(message: any, ...optionalParams: any[]) {
    const stack = optionalParams[0];
    logger.info(`LOG [${stack}]: ${message}`);
    super.log(message, ...optionalParams);
  }
}
